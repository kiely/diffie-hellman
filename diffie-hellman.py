a = 78393 # A's key
b = 23892 # B's key
g = 2011 # Small Generator Prime
n = 98764321234 # Large Prime

print ("generator: ", g)
print ("prime: ", n)

# Generate pre-master key
amod = (g**a) % n
bmod = (g**b) % n

print("a public: ", amod)
print("b public: ", bmod)

# Generate shared secret
aout = (amod**b) % n
bout = (bmod**a) % n

print("\n======== keep private ========")
if aout == bout:
    print("combined secret:  ", aout)
else:
    print("failed at creating secret")